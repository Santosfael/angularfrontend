import { Component, OnInit } from '@angular/core';
import { Cliente} from '../cliente';

@Component({
  selector: 'app-cliente-list',
  templateUrl: './cliente-list.component.html',
  styleUrls: ['./cliente-list.component.css']
})
export class ClienteListComponent implements OnInit {

  constructor() { }

  clientes: Cliente[] = [];

  ngOnInit() {
    this.clientes=[{
      'codigo': 1,
      'nome': 'Rafael',
    },
    {
      'codigo': 2,
      'nome': 'Rodrigo',  
    }];
  }
}
